json.array!(@jobs) do |job|
  json.extract! job, :id, :job_name, :job_due_date, :location
  json.url job_url(job, format: :json)
end
